import fancyCar from './img/fancyCar.jpg';

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Dealership Management</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
      <div className="car">
        <img src={fancyCar} alt='A fancy car' />
      </div>
    </div>
  );
}

export default MainPage;
